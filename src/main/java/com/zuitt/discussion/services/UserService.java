package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UserService {
    ResponseEntity createUser(User user);
    Iterable<User> getUsers();
    ResponseEntity deleteUser(Long id);

    ResponseEntity updateUser(Long id,User user);
}
