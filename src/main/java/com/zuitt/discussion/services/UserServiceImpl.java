package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserRepository userRepository;
    @Override
    public ResponseEntity createUser(User user) {
        userRepository.save(user);
        return new ResponseEntity<>("User created Succesfully", HttpStatus.CREATED);

    }
    public Iterable<User> getUsers(){
       return userRepository.findAll();
    }

    public ResponseEntity deleteUser(Long id){
        userRepository.deleteById(id);

        return new ResponseEntity<>("User Successfully Deleted",HttpStatus.OK);
    }
    public ResponseEntity updateUser(Long id, User user){
        User temp=userRepository.findById(id).get();
        temp.setUsername(user.getUsername());
        temp.setPassword(user.getPassword());

        userRepository.save(temp);

        return new ResponseEntity<>("User Successfully Updated",HttpStatus.OK);
    }
}
